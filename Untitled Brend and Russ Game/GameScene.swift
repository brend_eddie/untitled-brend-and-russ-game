//
//  GameScene.swift
//  Untitled Brend and Russ Game
//
//  Created by Brendan McKinley on 8/16/20.
//  Copyright © 2020 Brendan McKinley. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var himbo:SKSpriteNode!
    
    override func didMove(to view: SKView) {
        
        himbo = SKSpriteNode(imageNamed: "circle")
        himbo.position = CGPoint(x : self.frame.size.width / 2, y : self.frame.size.height / 2)
        
        self.addChild(himbo)
        
    }
    
    
    func touchDown(atPoint pos : CGPoint) {
        
    }
    
    func touchUp(atPoint pos : CGPoint) {
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
